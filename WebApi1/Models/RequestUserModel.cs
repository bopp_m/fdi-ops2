﻿namespace WebApi1.Models
{
    public class RequestUserModel
    {
        public string Username { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string MainLanguage { get; set; } = null!;
        public string TargetLanguage { get; set; } = null!;
        public string Niveau { get; set; } = null!;
    }
}
