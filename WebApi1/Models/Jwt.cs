﻿namespace WebApi1.Models
{
    public class Jwt
    {
        public int Id { get; set; } 
        public string email { get; set; }
        public string role { get; set; }
        public string nbf { get; set; }
        public string exp { get; set; }
        public string iat { get; set; }
        public string? aud { get; set; }
        public string iss { get; set; }
    }
}
