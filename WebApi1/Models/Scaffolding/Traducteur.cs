﻿using System;
using System.Collections.Generic;

namespace WebApi1.Models.Scaffolding
{
    public partial class Traducteur
    {
        public int TraducteurId { get; set; }
        public string Username { get; set; } = null!;
        public string Email { get; set; } = null!;
        public string Password { get; set; } = null!;
        public TimeSpan DateCreation { get; set; }
        public string MainLanguage { get; set; } = null!;
        public string? Certifications { get; set; }
        public string? UserId { get; set; }
    }
}
