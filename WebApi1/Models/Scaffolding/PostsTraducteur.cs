﻿using System;
using System.Collections.Generic;

namespace WebApi1.Models.Scaffolding
{
    public partial class PostsTraducteur
    {
        public int PostId { get; set; }
        public int TraducteurId { get; set; }
        public string MainLanguageId { get; set; } = null!;
        public string? Titre { get; set; }
        public string? Descr { get; set; }
        public string Localisation { get; set; } = null!;
    }
}
