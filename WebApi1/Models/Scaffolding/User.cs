﻿using System;
using System.Collections.Generic;

namespace WebApi1.Models.Scaffolding
{
    public partial class User
    {
        public string Username { get; set; } = null!;
        public int UserId { get; set; }
        public string Password { get; set; } = null!;
        public string Email { get; set; } = null!;
        public TimeSpan DateCreation { get; set; }
        public string MainLanguage { get; set; } = null!;
        public string TargetLanguage { get; set; } = null!;
        public string Niveau { get; set; } = null!;
        public string? TraducteurId { get; set; }
    }
}
