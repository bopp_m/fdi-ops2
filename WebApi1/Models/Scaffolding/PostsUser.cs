﻿using System;
using System.Collections.Generic;

namespace WebApi1.Models.Scaffolding
{
    public partial class PostsUser
    {
        public int PostId { get; set; }
        public int UserId { get; set; }
        public int? IdTypeDocument { get; set; }
        public int LangueTargetId { get; set; }
        public string? Titre { get; set; }
        public string? Descr { get; set; }
        public string? Localisation { get; set; }
    }
}
