﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApi1.Models.Scaffolding
{
    public partial class TrabiBDDContext : DbContext
    {
        public TrabiBDDContext()
        {
        }

        public TrabiBDDContext(DbContextOptions<TrabiBDDContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Language> Languages { get; set; } = null!;
        public virtual DbSet<PostsTraducteur> PostsTraducteurs { get; set; } = null!;
        public virtual DbSet<PostsUser> PostsUsers { get; set; } = null!;
        public virtual DbSet<Traducteur> Traducteurs { get; set; } = null!;
        public virtual DbSet<User> Users { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=tcp:trabidb.database.windows.net,1433;Initial Catalog=TrabiBDD;Persist Security Info=False;User ID=etna;Password=rootroot42*;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Language>(entity =>
            {
                entity.HasKey(e => e.LanguId);

                entity.Property(e => e.LanguId)
                    .ValueGeneratedNever()
                    .HasColumnName("languId");

                entity.Property(e => e.Langage)
                    .HasMaxLength(10)
                    .HasColumnName("langage")
                    .IsFixedLength();
            });

            modelBuilder.Entity<PostsTraducteur>(entity =>
            {
                entity.HasKey(e => e.PostId);

                entity.ToTable("Posts_Traducteurs");

                entity.Property(e => e.PostId)
                    .ValueGeneratedNever()
                    .HasColumnName("postId");

                entity.Property(e => e.Descr)
                    .HasMaxLength(10)
                    .HasColumnName("descr")
                    .IsFixedLength();

                entity.Property(e => e.Localisation)
                    .HasMaxLength(10)
                    .HasColumnName("localisation")
                    .IsFixedLength();

                entity.Property(e => e.MainLanguageId)
                    .HasMaxLength(10)
                    .HasColumnName("mainLanguageId")
                    .IsFixedLength();

                entity.Property(e => e.Titre)
                    .HasMaxLength(10)
                    .HasColumnName("titre")
                    .IsFixedLength();

                entity.Property(e => e.TraducteurId).HasColumnName("traducteurId");
            });

            modelBuilder.Entity<PostsUser>(entity =>
            {
                entity.HasKey(e => e.PostId);

                entity.ToTable("Posts_Users");

                entity.Property(e => e.PostId)
                    .ValueGeneratedNever()
                    .HasColumnName("postId");

                entity.Property(e => e.Descr)
                    .HasMaxLength(100)
                    .HasColumnName("descr")
                    .IsFixedLength();

                entity.Property(e => e.IdTypeDocument).HasColumnName("idTypeDocument");

                entity.Property(e => e.LangueTargetId).HasColumnName("langueTargetId");

                entity.Property(e => e.Localisation)
                    .HasMaxLength(10)
                    .HasColumnName("localisation")
                    .IsFixedLength();

                entity.Property(e => e.Titre)
                    .HasMaxLength(10)
                    .HasColumnName("titre")
                    .IsFixedLength();

                entity.Property(e => e.UserId).HasColumnName("userId");
            });

            modelBuilder.Entity<Traducteur>(entity =>
            {
                entity.Property(e => e.TraducteurId).ValueGeneratedNever();

                entity.Property(e => e.Certifications)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.Email)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.MainLanguage)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Password)
                    .HasMaxLength(64)
                    .IsFixedLength();

                entity.Property(e => e.UserId)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.Username)
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.UserId)
                    .ValueGeneratedNever()
                    .HasColumnName("userId");

                entity.Property(e => e.DateCreation).HasColumnName("dateCreation");

                entity.Property(e => e.Email)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.MainLanguage)
                    .HasMaxLength(30)
                    .HasColumnName("mainLanguage")
                    .IsFixedLength();

                entity.Property(e => e.Niveau)
                    .HasMaxLength(30)
                    .HasColumnName("niveau")
                    .IsFixedLength();

                entity.Property(e => e.Password)
                    .HasMaxLength(64)
                    .IsFixedLength();

                entity.Property(e => e.TargetLanguage)
                    .HasMaxLength(30)
                    .HasColumnName("targetLanguage")
                    .IsFixedLength();

                entity.Property(e => e.TraducteurId)
                    .HasMaxLength(10)
                    .HasColumnName("traducteurId")
                    .IsFixedLength();

                entity.Property(e => e.Username)
                    .HasMaxLength(20)
                    .IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
