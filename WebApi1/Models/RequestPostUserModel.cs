﻿namespace WebApi1.Models
{
    public class RequestPostUserModel
    {
        public int? IdTypeDocument { get; set; }
        public int LangueTargetId { get; set; }
        public string? Titre { get; set; }
        public string? Descr { get; set; }
        public string? Localisation { get; set; }
    }
}
