﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WebApi1.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    languId = table.Column<int>(type: "int", nullable: false),
                    langage = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.languId);
                });

            migrationBuilder.CreateTable(
                name: "Posts_Traducteurs",
                columns: table => new
                {
                    postId = table.Column<int>(type: "int", nullable: false),
                    traducteurId = table.Column<int>(type: "int", nullable: false),
                    mainLanguageId = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: false),
                    titre = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
                    descr = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
                    localisation = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts_Traducteurs", x => x.postId);
                });

            migrationBuilder.CreateTable(
                name: "Posts_Users",
                columns: table => new
                {
                    postId = table.Column<int>(type: "int", nullable: false),
                    userId = table.Column<int>(type: "int", nullable: false),
                    idTypeDocument = table.Column<int>(type: "int", nullable: true),
                    langueTargetId = table.Column<int>(type: "int", nullable: false),
                    titre = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true),
                    descr = table.Column<string>(type: "nchar(100)", fixedLength: true, maxLength: 100, nullable: true),
                    localisation = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts_Users", x => x.postId);
                });

            migrationBuilder.CreateTable(
                name: "Traducteurs",
                columns: table => new
                {
                    TraducteurId = table.Column<int>(type: "int", nullable: false),
                    Username = table.Column<string>(type: "nchar(20)", fixedLength: true, maxLength: 20, nullable: false),
                    Email = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: false),
                    Password = table.Column<string>(type: "nchar(64)", fixedLength: true, maxLength: 64, nullable: false),
                    DateCreation = table.Column<TimeSpan>(type: "time", nullable: false),
                    MainLanguage = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: false),
                    Certifications = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: true),
                    UserId = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Traducteurs", x => x.TraducteurId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    userId = table.Column<int>(type: "int", nullable: false),
                    Username = table.Column<string>(type: "nchar(20)", fixedLength: true, maxLength: 20, nullable: false),
                    Password = table.Column<string>(type: "nchar(64)", fixedLength: true, maxLength: 64, nullable: false),
                    Email = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: false),
                    dateCreation = table.Column<TimeSpan>(type: "time", nullable: false),
                    mainLanguage = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: false),
                    targetLanguage = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: false),
                    niveau = table.Column<string>(type: "nchar(30)", fixedLength: true, maxLength: 30, nullable: false),
                    traducteurId = table.Column<string>(type: "nchar(10)", fixedLength: true, maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.userId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Posts_Traducteurs");

            migrationBuilder.DropTable(
                name: "Posts_Users");

            migrationBuilder.DropTable(
                name: "Traducteurs");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
