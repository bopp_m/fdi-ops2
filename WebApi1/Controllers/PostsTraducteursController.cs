﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.IdentityModel.Tokens.Jwt;
using WebApi1.Models;
using WebApi1.Models.Scaffolding;

namespace WebApi1.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize]
    public class PostsTraducteursController : Controller
    {
        private readonly TrabiBDDContext _context;

        public PostsTraducteursController(TrabiBDDContext context)
        {
            _context=context;
        }
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Index()
        {
            return Ok(_context.PostsTraducteurs);
        }
        [HttpGet]
        public IActionResult Details(int id)
        {
            PostsTraducteur? post = _context.PostsTraducteurs.Where(x => x.TraducteurId == id).FirstOrDefault();
            if (post == null)
            { return BadRequest("No element for post with id :" + id); }
            HttpContext context = HttpContext;
            if (Authorized(post.TraducteurId, context) == false)
            {
                return Unauthorized();
            };
            return Ok(post);
        }
        [HttpPost]
        public IActionResult Create(RequestPostTraducteurModel post)
        {
            HttpContext context = HttpContext;
            var handler = new JwtSecurityTokenHandler();
            context.Request.Headers.TryGetValue("Authorization", out var authorizationStream);
            var jsonToken = handler.ReadToken(authorizationStream.ToString().Substring(7));
            var tokenS = jsonToken as JwtSecurityToken;
            var TraducteurId = tokenS.Payload["Id"];

            PostsTraducteur newPost = new PostsTraducteur()
            {
                PostId = _context.PostsTraducteurs.OrderByDescending(x => x.PostId).FirstOrDefault() != null ? _context.PostsTraducteurs.OrderByDescending(x => x.PostId).First().PostId + 1 : 1,
                //Date = DateTime.Now.ToString().Substring(0, 10),
                TraducteurId = Int32.Parse(TraducteurId.ToString()),
                Titre = post.Titre,
                Descr = post.Description
            };
            try
            {
                _context.PostsTraducteurs.Add(newPost);
                _context.SaveChanges();
                return Ok(newPost);
            }
            catch (Exception err)
            {
                return BadRequest("error occured : "+ err);
            }
        }
        [HttpPut("Id")]
        public IActionResult Edit(int Id, RequestPostTraducteurModel postToEdit)
        {
            PostsTraducteur? post = _context.PostsTraducteurs.Where(x => x.PostId == Id).FirstOrDefault();
            if (post == null)
            { return BadRequest("No element for post with id :" + Id); }
            HttpContext context = HttpContext;
            if (Authorized(post.TraducteurId, context) == false)
            {
                return Unauthorized();
            };
            try
            {
                _context.PostsTraducteurs.Update(post);
                _context.SaveChanges();
                return Ok(post);
            }
            catch (Exception err)
            {
                return BadRequest("An error occurred : " + err.Message);
            }
        }
        [HttpDelete]
        public IActionResult Delete(int id)
        {
            PostsTraducteur? post = _context.PostsTraducteurs.Where(x => x.PostId == id).FirstOrDefault();
            if (post == null)
            {
                return BadRequest("No element for id : " + id);
            }
            HttpContext context = HttpContext;
            if (Authorized(post.TraducteurId, context) == false)
            {
                return Unauthorized();
            };
            try { _context.PostsTraducteurs.Remove(post);
                _context.SaveChanges();
                return Ok("Succesfully Deleted.");
            }
            catch (Exception err)
            {
                return Ok(err);
            }

        }

        private bool Authorized(int Id, HttpContext context)
        {
            context.Request.Headers.TryGetValue("Authorization", out var authorizationStream);
            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(authorizationStream.ToString().Substring(7));
            var tokenS = jsonToken as JwtSecurityToken;
            var TraducteurId = tokenS.Payload["Id"];
            var roleUser = tokenS.Payload["role"];
            if (Id.ToString() != TraducteurId.ToString() && roleUser.ToString().Contains("admin") == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
