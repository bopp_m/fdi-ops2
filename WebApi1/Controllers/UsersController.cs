﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mail;
using System.Text;
using WebApi1.Models;
using WebApi1.Models.Scaffolding;
using XSystem.Security.Cryptography;
using System.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication;
using Newtonsoft.Json;

namespace WebApi1.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly TrabiBDDContext _context;
        private readonly IConfiguration _conf;

        public UsersController(IConfiguration conf, TrabiBDDContext context)
        {
            _context=context;
            _conf=conf;
        }

        [HttpGet("/Users")]
        [AllowAnonymous]
        public IQueryable<User> Index()
        {
            return _context.Users;
        }

        [HttpGet("/Users/{id}")]
        public IActionResult Id(int id)
        {
            HttpContext context = HttpContext;
            if (Authorized(id, context) == false)
            {
                return Unauthorized();
            };
            try
            {
                return Ok(_context.Users.Where(x => x.UserId == id).First());
            }
            catch (Exception err)
            {
                return BadRequest("An error occurred : No user for this Id");
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Create(RequestUserModel request)
        {
            if (IsEmailValid(request.Email) == false)
            {
                return BadRequest("Please input good email format");
            }
            try
            {
                User user = new User()
                {
                    UserId = _context.Users.OrderByDescending(x => x.UserId).FirstOrDefault() != null ? _context.Users.OrderByDescending(x => x.UserId).First().UserId + 1 : 1,
                    MainLanguage = request.MainLanguage,
                    DateCreation = DateTime.UtcNow.TimeOfDay,
                    Niveau = request.Niveau,
                    Username = request.Username,
                    TargetLanguage = request.TargetLanguage,
                    Email = request.Email,
                    Password = Hash(request.Password),
                    //role = request.role
                };
                _context.Users.Add(user);
                _context.SaveChanges();
                return Ok(user);
            }
            catch (Exception err)
            { return BadRequest("An error occurred : " + err); }
        }

        [HttpPut("{Id}")]
        public IActionResult Update(int Id, RequestUserModel user)
        {
            HttpContext context = HttpContext;
            if (Authorized(Id, context) == false)
            {
                return Unauthorized("You dont have enought rights for doing this action.");
            };

            if (IsEmailValid(user.Email) == false)
            {
                return BadRequest("Please input good email format");
            }
            User? userToUpdate = _context.Users.Where(x => x.UserId == Id).FirstOrDefault();
            if (userToUpdate == null)
            {
                return BadRequest("No user with this Id.");
            }
            try
            {
                
                userToUpdate.Email = user.Email;
                userToUpdate.Password = Hash(user.Password);
                //userToUpdate.Role = user.role;
                _context.Users.Update(userToUpdate);
                _context.SaveChanges();
                return Ok(user);
            }
            catch (Exception err)
            {
                return BadRequest("An error occurred : : " + err.Message);
            }
        }

        [HttpDelete("{Id}")]
        public IActionResult Delete(int Id)
        {
            HttpContext context = HttpContext;
            if (Authorized(Id, context) == false)
            {
                return Unauthorized();
            };
            User? user = _context.Users.Where(x => x.UserId == Id).FirstOrDefault();
            if (user == null)
            {
                return BadRequest("No user with this Id.");
            }
            try
            {
                
                _context.Users.Remove(user);
                _context.SaveChanges();
                return Ok("Succesfully deleted.");
            }
            catch (Exception err)
            {
                return BadRequest("An error occurred :" + err);
            }
        }

        [HttpGet("/Users/Authenticate")]
        [AllowAnonymous]
        public IActionResult authent(string email, string password)
        {
            //RECUPERE LE USER AVEC MAIL CORRESPONDANT
            User? user = _context.Users.Where(x => x.Email == email).FirstOrDefault();
            //RETURN BADREQUEST SI INEXISTANT
            if (user == null)
            {
                return BadRequest("No user for that email.");
            }
            string pwd = Hash(password);
            //VERIFICATION CORRESPONDANCE PWD HASHé
                if (pwd == user.Password)
            {
                //GENERATION JWT TOKEN
                var issuer = _conf["Jwt:Issuer"];
                var audience = _conf["Jwt:Audience"];
                var key = Encoding.ASCII.GetBytes
                (_conf["Jwt:Key"]);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                    {
            new Claim("Id", user.UserId.ToString()),
            new Claim(ClaimTypes.Email, user.Email),
            //new Claim(ClaimTypes.Role, user.Role.Trim()),
            }),
                    Expires = DateTime.UtcNow.AddMinutes(5),
                    Issuer = issuer,
                    Audience = audience,
                    SigningCredentials = new SigningCredentials
                    (new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha512Signature)
                };
                // ECRITURE JWT TOKEN
                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var stringToken = tokenHandler.WriteToken(token);

                return Ok(Results.Ok(stringToken));
            }
                //SI LE MDP NEST PAS CORRESPONDANT
            return Unauthorized("invalid credentials.");
        }

        //FONCTION DE HASHAGE DE STRING
        private string Hash(string hashBase)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(hashBase);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        //VERIFICATION FORMAT EMAIL
        private bool IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        //CUSTOM AUTHENTICATION MIDDLEWARE 
        private bool Authorized(int Id, HttpContext context)
        {
            //RECUPERATION TOKEN AUTHENT SI EXISTANT
            context.Request.Headers.TryGetValue("Authorization", out var authorizationStream);

            var handler = new JwtSecurityTokenHandler();
            //RECUPERATION TOKEN VALUE SANS BEARER
            var jsonToken = handler.ReadToken(authorizationStream.ToString().Substring(7));
            var tokenS = jsonToken as JwtSecurityToken;
            //RECUPERATION ID ET ROLE USER
            var idUser = tokenS.Payload["Id"];
            var roleUser = tokenS.Payload["role"];
            if (Id.ToString() != idUser.ToString() && roleUser.ToString().Contains("admin") == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
